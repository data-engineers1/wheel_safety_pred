from ultralytics import YOLO
import cv2
from pathlib import Path
import time
from  ultralytics.utils.plotting import Annotator
model = YOLO("/home/milestone/Downloads/yolov8_detect.pt")

video_path = "/home/milestone/Downloads/img-7598-oo.mp4"
cap = cv2.VideoCapture(video_path)

frame_rate = int(cap.get(cv2.CAP_PROP_FPS))
output_path = "file-output0000.mp4"
frame_width = int(cap.get(3))
frame_height = int(cap.get(4))
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter(output_path, fourcc, frame_rate, (frame_width, frame_height))

while cap.isOpened():
    success, frame = cap.read()

    if success:
        results = model(frame, conf = 0.40)
        for result in results:
            annotator = Annotator(frame)
            boxes = result.boxes

            for box in boxes:
                b = box.xyxy[0]
                print(b,frame_width,frame_height)
                # Dynamic AOI definition based on the image dimensions
                aoi_width = 0.95 * frame_width  # AOI width as 40% of the image width
                aoi_height = 0.99 * frame_height  # AOI height as 60% of the image height

                # Calculate AOI coordinates
                aoi_xmin = (frame_width - aoi_width) / 2
                aoi_ymin = (frame_height - aoi_height) / 2
                aoi_xmax = aoi_xmin + aoi_width
                aoi_ymax = aoi_ymin + aoi_height
                frame = cv2.rectangle(frame, (int(aoi_xmin), int(aoi_ymin)), (int(aoi_xmax), int(aoi_ymax)), (0, 255, 255), 10)  # (0, 255, 0) is the color in BGR format, 2 is the thickness
                if b[0] > aoi_xmin and b[2] > aoi_ymin and b[1] < aoi_ymax and b[3] < aoi_ymax:
                    frame = cv2.rectangle(frame, (int(b[0]), int(b[1])), (int(b[2]), int(b[3])), (0, 255, 0), 10)  # (0, 255, 0) is the color in BGR format, 2 is the thickness

                c = box.cls
                annotator.box_label(b, model.names[int(c)])

            frame = annotator.result()
        frame = cv2.resize(frame, (640,480))
        cv2.imshow('frame', frame)
        if cv2.waitKey(1) == ord('q'):
            break        
            # out.write(frame)
    else:
        break

cap.release()
out.release()
cv2.destroyAllWindows()
