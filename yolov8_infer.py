from ultralytics import YOLO
import cv2
from pathlib import Path
import time
import numpy as np
from  ultralytics.utils.plotting import Annotator
model = YOLO("/home/milestone/Downloads/yolov8_seg.pt")
model_det = YOLO("/home/milestone/Downloads/yolov8_detect.pt")

video_path = "/home/milestone/Downloads/output/input_video/IMG_7596.MP4"
cap = cv2.VideoCapture(video_path)

frame_rate = int(cap.get(cv2.CAP_PROP_FPS))
output_path = "file-output0000.mp4"
frame_width = int(cap.get(3))
frame_height = int(cap.get(4))
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter(output_path, fourcc, frame_rate, (frame_width, frame_height))

def classify(img_detail):
    bbox=img_detail['bbox']
    classes=img_detail['classes']
    confidence=img_detail['confidence']
    height=img_detail['height']
    width=img_detail['width']
    my_dict = {}
    result=[]
    target_folder='safe'
    for i in zip(bbox,classes,confidence):
        bx=i[0][0]
        by=i[0][1]
        bw=i[0][2]
        bh=i[0][3]

        
        bbox_center_x=bx+(bw/2)
        bbox_center_y=by+(bh/2)
        image_center_x=width/2
        image_center_y=height/2
        distance = np.sqrt(((bbox_center_x-image_center_x)**2)+((bbox_center_y-image_center_y)**2))
        
        classes = str(i[1][0]).split('.')[0]
        confidence = i[2][0]
        tuple_for_iteration = (classes, confidence)
        my_dict[distance]=tuple_for_iteration
    my_dict=dict(sorted(my_dict.items()))
    
    #7 closet nuts 
    first_7_items = dict(list(my_dict.items())[:7])
    #5 hightest confidence
    dictt=dict(list(sorted(first_7_items.items(), key=lambda item: item[1][1]))[:5])

    final_classes = list(map(lambda x: str(x[0]), dictt.values()))
    if final_classes:
        print("kdjgv",final_classes)
        is_safe = all(element == '0' for element in final_classes)
        target_folder = "safe" if is_safe else "unsafe"
    else:
        target_folder='-'
    return target_folder



while cap.isOpened():
    success, frame = cap.read()

    if success:
        results = model(frame, conf = 0.80)
        results_det = model_det(frame, conf = 0.80)
        classes=[]
        bbox=[]
        img_detail={}
        confi=[]
        for result in results:
            annotator = Annotator(frame)
            boxes = result.boxes
            for box in boxes:
                box1 = box.xyxy.tolist()
                conf = box.conf.tolist()
                cls = box.cls.tolist()
                
                # left, top, width, height = convert_to_top_left(box[0],box[1],box[2],box[3])
            
                # print(left, top, width, height)
        # break
        
        for result in results:
            annotator = Annotator(frame)
            boxes = result.boxes

            for box in boxes:
                b = box.xyxy[0]
                bb = box.xyxy[0].tolist()
                bbox.append(bb)
                c = box.cls
                clss = box.cls.tolist()
                classes.append(clss)
                conf = box.conf.tolist()
                confi.append(conf)
                annotator.box_label(b, model.names[int(c)])

            frame = annotator.result()
        height, width, channels = frame.shape
        my_dict = {
            'bbox': bbox,
            'classes': classes,
            'confidence': confi,
            'height':height,
            'width':width
         }
        target_folder=classify(my_dict)
       
        text_height_percentage = 0.01  # Adjust as needed
        height, width, channels = frame.shape
        text_size = int(height * text_height_percentage)
        
        (text_width, text_height), _ = cv2.getTextSize(target_folder, cv2.FONT_HERSHEY_SIMPLEX, text_size/2, 10)
        #  Calculate the position for the text
        x_position = int((width - text_width) / 2)
        y_position = int((height + text_height) / 2) - int(height / 3)
        # # Adjust the background rectangle position based on the text position
        background_rect_coords = ((x_position, y_position - text_height), 
                                (x_position + text_width, y_position + 10))  # Adjust padding (10) if needed

        image_with_text = cv2.rectangle(frame.copy(), *background_rect_coords, (0, 0, 0), -1)
        cv2.putText(image_with_text, target_folder, (x_position, y_position), cv2.FONT_HERSHEY_SIMPLEX, text_size/2, (255, 255, 255), 3)
        frame = cv2.resize(image_with_text, (640,480))
        cv2.imshow('frame', image_with_text)
        if cv2.waitKey(1) == ord('q'):
            break        
        # out.write(frame)
    else:
        break

cap.release()
out.release()
cv2.destroyAllWindows()