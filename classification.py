from ultralytics import YOLO
import cv2
from pathlib import Path
import time
import numpy as np
from  ultralytics.utils.plotting import Annotator
from datetime import datetime,date
import pandas as pd

def wheel_detection_points(frame,results,model):
    bounding_boxes = []
    for result in results:
        annotator = Annotator(frame)
        boxes = result.boxes

        for box in boxes:
            
            
            b = box.xyxy[0]
            bb = b.tolist()
            c = box.cls
            xmin, ymin, xmax, ymax = int(bb[0]),int(bb[1]),int(bb[2]),int(bb[3])
            
            bounding_boxes.append((xmin, ymin, xmax, ymax))
    if bounding_boxes:
        return bounding_boxes
        

def prediction_result(frame,results,model,model_seg):
    classes=[]
    bbox=[]
    img_detail={}
    confi=[]
    x, y, w, h=None,None,None,None
    for result in results:
        annotator = Annotator(frame)
        boxes = result.boxes

        for box in boxes:
            
            
            b = box.xyxy[0]
            bb = b.tolist()
            c = box.cls
            x, y, w, h = int(bb[0]),int(bb[1]),int(bb[2]),int(bb[3])
            # annotator.box_label(b, model.names[int(c)])

        frame = annotator.result()
    croped=frame[y:h,x:w]
    image = np.array(croped, dtype=np.uint8)

    results_seg = model_seg(image, conf = 0.40)
    for result in results_seg:
    
        annotator = Annotator(image)
        boxes = result.boxes

        for box in boxes:
            b = box.xyxy[0]
            bb = b.tolist()
            bbox.append(bb)
            c = box.cls
            clss = c.tolist()
            classes.append(clss)
            conf = box.conf.tolist()
            confi.append(conf)
            annotator.box_label(b, model_seg.names[int(c)])
        frame1 = annotator.result()
    height, width, channels = frame1.shape
    my_dict = {
        'bbox': bbox,
        'classes': classes,
        'confidence': confi,
        'height':height,
        'width':width
    }
    return height,width,my_dict

def classify(img_detail):
    bbox=img_detail['bbox']
    classes=img_detail['classes']
    confidence=img_detail['confidence']
    height=img_detail['height']
    width=img_detail['width']
    my_dict = {}
    result=[]
    target_folder='safe'
    for i in zip(bbox,classes,confidence):
        bx=i[0][0]
        by=i[0][1]
        bw=i[0][2]
        bh=i[0][3]

        
        bbox_center_x=bx+(bw/2)
        bbox_center_y=by+(bh/2)
        image_center_x=width/2
        image_center_y=height/2
        distance = np.sqrt(((bbox_center_x-image_center_x)**2)+((bbox_center_y-image_center_y)**2))
        
        classes = str(i[1][0]).split('.')[0]
        confidence = i[2][0]
        tuple_for_iteration = (classes, confidence)
        my_dict[distance]=tuple_for_iteration
    my_dict=dict(sorted(my_dict.items()))
    
    #7 closet nuts 
    first_7_items = dict(list(my_dict.items())[:7])
    #5 hightest confidence
    dictt=dict(list(sorted(first_7_items.items(), key=lambda item: item[1][1]))[:5])

    final_classes = list(map(lambda x: str(x[0]), dictt.values()))
    if final_classes:
        
        is_safe = all(element == '0' for element in final_classes)
        target_folder = "safe" if is_safe else "unsafe"
    else:
        target_folder='-'
    return target_folder

def write_text(height,width,frame,target_folder):
    text_height_percentage = 0.01  # Adjust as needed
    # height, width, channels = frame.shape
    text_size = int(height * text_height_percentage)
    
    (text_width, text_height), _ = cv2.getTextSize(target_folder, cv2.FONT_HERSHEY_SIMPLEX, text_size/2, 10)
    #  Calculate the position for the text
    x_position = int((width - text_width) / 2)
    y_position = int((height + text_height) / 2) - int(height / 3)
    # # Adjust the background rectangle position based on the text position
    background_rect_coords = ((x_position, y_position - text_height), 
                            (x_position + text_width, y_position + 10))  # Adjust padding (10) if needed

    image_with_text = cv2.rectangle(frame.copy(), *background_rect_coords, (0, 0, 0), -1)
    cv2.putText(image_with_text, target_folder, (x_position, y_position), cv2.FONT_HERSHEY_SIMPLEX, text_size/2, (255, 255, 255), 3)
    
    return image_with_text
annotated_frame = None
def track_wheel(model,image_with_text,target_folder,c,current_year,current_month,current_day,df,frame_height,frame_width):
    # Dynamic AOI definition based on the image dimensions
    aoi_width = 0.95 * frame_width  # AOI width as 40% of the image width
    aoi_height = 0.9 * frame_height  # AOI height as 60% of the image height

    # Calculate AOI coordinates
    aoi_xmin = (frame_width - aoi_width) / 2
    aoi_ymin = (frame_height - aoi_height) / 2
    aoi_xmax = aoi_xmin + aoi_width
    aoi_ymax = aoi_ymin + aoi_height
    frame = cv2.rectangle(image_with_text, (int(aoi_xmin), int(aoi_ymin)), (int(aoi_xmax), int(aoi_ymax)), (0, 255, 255), 10)  # (0, 255, 0) is the color in BGR format, 2 is the thickness
    resultss = model.track(frame, persist=True,conf = 0.70)
    b = resultss[0].boxes.xyxy
    xmin=ymin=xmax=ymax=int()
    if b.numel() > 0:
        xmin = b[0, 0].item()
        ymin = b[0, 1].item()
        xmax = b[0, 2].item()
        ymax = b[0, 3].item()

    print("xmin:", xmin)
    print("ymin:", ymin)
    print("xmax:", xmax)
    print("ymax:", ymax)
    # global annotated_frame 
    # if xmin > aoi_xmin and ymin > aoi_ymin and xmax < aoi_ymax and ymax < aoi_ymax: 
    #     annotated_frame = resultss[0].plot()
    #     track_ids = None
    #     if resultss and resultss[0].boxes.id is not None:
    #         track_ids = resultss[0].boxes.id.int().cpu().tolist()
    #         # Rest of your code using track_ids
    #     else:
    #         # Handle the case where results or results[0].boxes.id is None
    #         print("Results are empty or results[0].boxes.id is None.")

    #     # Create a DataFrame with the new rows to be appended


    #     # Displays Time
    #     current_time = c.strftime('%H:%M:%S')
    #     if track_ids and target_folder != '-':

    #         new_rows = pd.DataFrame([{'wheel_id': i, 'class': target_folder,"Year":current_year,"Month":current_month,"Day":current_day,"Time":current_time} for i in track_ids if i not in df['wheel_id'].values])
            
            
    #         # Concatenate the original DataFrame and the new rows
    #         df = pd.concat([df, new_rows], ignore_index=True)
    # return annotated_frame,df

