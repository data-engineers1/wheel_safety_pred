from ultralytics import YOLO
import cv2
from pathlib import Path
import time
from  ultralytics.utils.plotting import Annotator
model = YOLO("/home/milestone/Downloads/ultralytics-main/models/yolov8_detect.pt")


image = cv2.imread("/home/milestone/Documents/tuck_wheel1.jpg")
results = model(image, conf = 0.40)
for result in results:
    annotator = Annotator(image)
    boxes = result.boxes
    for box in boxes:
        b = box.xyxy[0]
        c = box.cls
        annotator.box_label(b, model.names[int(c)])

    frame = annotator.result()
    frame = cv2.resize(frame, (640,480))
    cv2.imshow('frame', frame)
    cv2.waitKey(0)
 
# Close all windows
    cv2.destroyAllWindows()

    
