from ultralytics import YOLO
import cv2
from pathlib import Path
import time
import numpy as np
import pandas as pd
from datetime import datetime,date
from multiprocessing import Process, Manager
import os
from classification import classify,write_text,prediction_result,track_wheel,wheel_detection_points
import cv2,sys,subprocess
import argparse
import multiprocessing
import threading
from queue import Queue
from queue import Empty
import json
# import boto3
import requests
import json
import base64


def numpy_array_to_base64(image_np):
    # Convert the NumPy array to a byte stream
    
    image_byte_array = image_np.tobytes()

    # Encode the byte stream to base64
    encoded_image = base64.b64encode(image_byte_array).decode("utf-8")

    return encoded_image

#Initialise the object tracker class

def YOLO_pipeline(args,data_queue):

    os.environ["KMP_DUPLICATE_LIB_OK"]="TRUE"



    model = YOLO("models/yolov8_detect_s.pt")
    model_seg = YOLO("models/yolov8_seg.pt")


    selection_dict = {'img': None, 'points selected': []}
    bboxes = []

    def open_cam_rtsp(uri, width, height, latency):
        gst_rtsp = ("rtspsrc location={} ! rtph264depay ! h264parse ! avdec_h264 ! videoconvert ! appsink").format(uri)
        gst_str = ('rtspsrc location={} latency={} ! '
                'rtph264depay ! h264parse ! omxh264dec ! '
                'nvvidconv ! '
                'video/x-raw, width=(int){}, height=(int){}, '
                'format=(string)BGRx ! '
                'videoconvert ! appsink').format(uri, latency, width, height)
        return cv2.VideoCapture(gst_rtsp, cv2.CAP_GSTREAMER)

    # def write_base64_to_file(base64_string):
    #     output_file = "output.txt"
    #     with open(output_file, "w") as file:
    #         file.write(base64_string)
        
    def open_cam_usb(dev, width, height):
        # We want to set width and height here, otherwise we could just do:
        #     return cv2.VideoCapture(dev)
        # v4l2src device=/dev/video{} ! '
        #                'video/x-raw, width=(int){}, height=(int){} ! '
        #                'videoconvert ! appsink
                
        gst_str = ('v4l2src device=/dev/video{} ! '
                'image/jpeg, framerate=30/1, width=(int){}, height=(int){} ! '
                'jpegparse ! '
                'jpegdec ! videoconvert ! appsink').format(dev, width, height)
        return cv2.VideoCapture(gst_str, cv2.CAP_GSTREAMER)


    def open_cam_onboard(width, height):
        gst_elements = str(subprocess.check_output('gst-inspect-1.0'))
        if 'nvcamerasrc' in gst_elements:
            # On versions of L4T prior to 28.1, add 'flip-method=2' into gst_str
            gst_str = ('nvcamerasrc ! '
                    'video/x-raw(memory:NVMM), '
                    'width=(int)2592, height=(int)1458, '
                    'format=(string)I420, framerate=(fraction)30/1 ! '
                    'nvvidconv ! '
                    'video/x-raw, width=(int){}, height=(int){}, '
                    'format=(string)BGRx ! '
                    'videoconvert ! appsink').format(width, height)
        elif 'nvarguscamerasrc' in gst_elements:
            gst_str = ('nvarguscamerasrc ! '
                    'video/x-raw(memory:NVMM), '
                    'width=(int)1920, height=(int)1080, '
                    'format=(string)NV12, framerate=(fraction)30/1 ! '
                    'nvvidconv flip-method=2 ! '
                    'video/x-raw, width=(int){}, height=(int){}, '
                    'format=(string)BGRx ! '
                    'videoconvert ! appsink').format(width, height)
        else:
            raise RuntimeError('onboard camera source not found!')
        return cv2.VideoCapture(gst_str, cv2.CAP_GSTREAMER)


    cap=None
    source_images=False
    today = str(date.today())
    c = datetime.now()
    today =today.split('-')
    current_year = today[0]
    current_month = today[1]
    current_day = today[2]
    video_name = None
    if args.results:
        out_results=args.results
        if not out_results.endswith("/"):
            out_results = args.results+'/'
            
            
            
    if args.video:
        cap = cv2.VideoCapture(args.video)
        width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
        height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
        # Get the frames per second (fps) and total number of frames
        fps = cap.get(cv2.CAP_PROP_FPS)
        video = args.video.rsplit("/")[-1]
        video_name = video
        # total_framess=total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

        # # Calculate the duration in seconds
        # duration_seconds = total_frames / fps

        # # Convert the duration to minutes and seconds
        # duration_minutes = int(duration_seconds // 60)
        # duration_seconds %= 60
        
        # if not total_frames%2==0:
        #     total_frames +=1
            
        # time_duration =  int((total_frames/2))
        
    

    elif args.source:
        if os.path.isdir(args.source):
            source_images = True
        else:
            source_images = False

    elif args.use_rtsp:
    # rtsp_uri = "rtsp://localhost:8554/stream1"
    # if rtsp_uri:
        
        cap = open_cam_rtsp(args.rtsp_uri,
                                    args.image_width,
                                    args.image_height,
                                    args.rtsp_latency)
        
        width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
        height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
    

    elif args.use_usb:
        cap = open_cam_usb(args.video_dev,
                            args.image_width,
                            args.image_height)
        width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
        height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)

    else:
        cap = open_cam_onboard(args.image_width,
                                args.image_height)
        width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
        height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)

    # For Video

    if cap:
        fps = cap.get(cv2.CAP_PROP_FPS)
        # total_framess=total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

        # # Calculate the duration in seconds
        # duration_seconds = total_frames / fps

        # # Convert the duration to minutes and seconds
        # duration_minutes = int(duration_seconds // 60)
        # duration_seconds %= 60
        
        # if not total_frames%2==0:
        #     total_frames +=1
            
        time_duration =  int((fps*3))
        
    
        if not cap.isOpened():
            sys.exit('Failed to open camera!')

        cv2.destroyAllWindows()
        ############################################################################
        frame_rate = int(cap.get(cv2.CAP_PROP_FPS))
        
        

        if video_name:
            
            output_path = out_results+video_name
        else:
            output_path = "./abc.mp4"
            
        frame_width = int(cap.get(3))
        frame_height = int(cap.get(4))
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        out = cv2.VideoWriter(output_path, fourcc, frame_rate, (frame_width, frame_height))
        df = pd.DataFrame(columns=['wheel_id', 'class','Year','Month','Day','Time'])
        d=[]
        count=0
        wheels = []
        vehicleResult = ""
        vehicleImageBase64 = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAASABIAAD/4QBYRXhpZgAATU0AKgAAAAgAAgESAAMAAAABAAEAAIdpAAQAAAABAAAAJgAAAAAAA6ABAAMAAAABAAEAAKACAAQAAAABAAABA6ADAAQAAAABAAAAwgAAAAD/wAARCADCAQMDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9sAQwACAgICAgIDAgIDBQMDAwUGBQUFBQYIBgYGBgYICggICAgICAoKCgoKCgoKDAwMDAwMDg4ODg4PDw8PDw8PDw8P/9sAQwECAgIEBAQHBAQHEAsJCxAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQ/90ABAAR/9oADAMBAAIRAxEAPwD8VEq8vWsxHq5XoAOtu1WXkqjbVabrWgEUz1NVR6StAJm6VFD0oeo0rMCR6ltu1UXqzD0oAfU1vUbyVEld+FpGZK3SpV61E3SpU/49vxr1KRmWUqTZQ9SPX0FI5w0a+/szW7LUv+feeOX/AL4YH+lfVVxo1tceLPG2j/8AQX0+O8i/4Gm//wBDevkiZK+s9J1O6ub7wR4h/wCgto1xp8v/AF0gRm/+Jr9j8NMVD34T7p/feL/NHxHFtKXuTh5r9V+R8jy3S22i/Z8c3NznPvbJx+k5qtofiDUvDesQ6vo0vlXcL7k7g/7LDup7j0r0CX4Z+ONbuW0/w3odzewLPdMJvLMcB2yGPb5zYjB3ION3f0IJ9E0P9lvxHcET+Ntf0/w3Z5AIL+fOp75UFIiP+21fyjm3E1GlP/aqqifsOV8KYzGf7rScvl+ux774Y+Jdt4z06HULBBGZflnQc+TNgH27HNbVhrpurizuZYv38Dggxblf5eRyDWNpUfwN+F1t9m8JabcX19+7825lkeSSSSP7jfJ5aL/up8lc1dfGDUILc22jxJEP4c53D/e2kbv0r5vibjrDYmcPqsHL+Z7L8bM/Qcn8JMZyf7VNUvxf4afifYOq6j8VvFv/ABMvCWmyaTpVv/x9Sy7/ALZcRxor/O/8K/K33P7teH+LNa8O6Lrlt+68iLUJJSZx1Eny5Z++1t3v06V5TqPxn8W+ILZk8QeIboLGTtiEhEL5bDKyDAK7eOR3rxzT9auT/o+oSvPDjYN7n5B/s9cV1/8AEQ8fR/d5fP2UftaLX/Lz1Ncm8M8NPn/tCHNLZWb0++1/LQ+89M8O3Vx5U/SE/wAf/wATXodhoemab/yx/wC2stfJnw++I974E8rT9ZlN74fk+6xyXT/d/wBn/Z/Ee/1e+uWv2aC5tpvPguP3kXlV/Q/h3xzDOIe58X8p+I8a8H1sqn7/AMHSR1miNBdLe+FrrPkyo0kLAdHxwMnoNwUgD1NfDfxgtdT03RND8W6J/wAf2gySaLL+78z/AEf79pvR/vfIzJ/v19Ot4gNvqlvqXeGQeZ/uH5G/ErxWZ8RfBdtc/btN87yIPEtvJHFc/wDLOO8g3S2027+Hy3Vv++lr9O4hyKWLwk8LD1j6/wDD/gfCZHnNHAY+GKn6fLZ/hsfKsPxK+Ktt4Jgtv3dhPBH5X265k/0y4j+4m1H+60f3d7/3areDPFup3Hxa8K+NvCUNzq2q+Xaf2lFFG8knmf8AHtc/c/hkhXf/AMDr0N/CvgbTbb/ittY/4SWf935tlpH+jWfmR/37l/30v3f4P++K6CHxNrn2b+xPBNnb+E9K/wCeVlH9m/77f77N/t/JX53/AMQ7xmJ5Pb1XGOm+ruvTT8z9F/1+wEOeGFw/NKV07aKz331Z6jolrc+Cfi1fW1t/x428nmy/vEjjjs7v/bf/AJ5/+y17H4htPsuslu0/P5jn/wAezXyPo/h+5/f/AL7z57j/AFvm19VR6ra6n4K0rUNRnS2mhHkSSSusf7yI7N+5zjJAD/jWnGvPyUfqv8WOnd2em3zR8jk/2+f7X5hso2Vl/wDCWeHP+ftP++H/APiaP+Es8Of8/af98P8A/E18Z/qTxJ/0Dz/D/M7/AO3cB/Oj/9D8R9lWKEor1AJUpN9RpUm+tDMj30b6HooAHejfUb1HsrM0JHerKVRdKloMyy9SJVLfXWaL4Q8YeI7cXWj6HeXdvv2GaOBzAG9HlxsX/gTCvR+tQh785G1LCzrT5IQ5jAer9e4aT8AtSEBm8baxBoGfL8tMLdyD5vm3KrovA6YkPPXvXYw+Evgb4b/4+Re+JJ/+nmTy4/8AviHy/wDx93rxcT4h5bR/hz5peSv+O34n3WV+F2cYn7HL6u34b/gfMMMd3c3EVvawyXE3/POJDI5+iqCT+Ve0WHwN+IepXP2aWzgsoYjgTXM6eWfwj8x//HK+kdKm+If2b7N4K8Hx6FY/89fL+xR/7++by0asO88MvcXGfG3xF0uwH/LWO2d72T/vzD5dfC4/xoxMpuGFpwj6vnf3I/Uct8EcBQ9/McQ5eit+d2ecW3wW8DaJ/wAjb4kknn/55WMaR/8Aj83mbv8Avha76Dxt4Q8MadBo/hPR/wBxb7zC8jF3j805cCSXc4z3556V694W+A/gfxHov9pW0+u6558nlR3MUljotn/Du+e/+997+B/4a3vHnw0/Zh+FF/8AYPFur6NDe29vHJLb3WvSahceZ/1x06KNP+A18zi+JMyx8JzdWdWP2owTinfpbS/nc+mwuH4eyqcKdOlCMv5nZv1u2fMniP4o+IpjFbLdGHCqOOD8y7j83XvXlN5rlzc/8fM0k9e3eHf2k/gN/bX/ABUfgXTreyt4444pZbRtRkuI4PkRf3n3V2Kteq2n7QP7J+oE2x8JaUZ7j/VRW2geXI/zfxec+z7n+r2V5GFweIor3MFL71/nY1q+IGEnyU/rC+X/AAx8Q/2iai+1XNe+fEa2+HviXUoT4Cs7PQ7g28kl1pvz2393a/z/ALlf93fXi0Mdt9pz/aWnf9srj7TJ/wB8J5j/APjlfSZbUlVhzqk4/wB1/wBfcznqZnS/5eVV96MO5e6+01L9nroL2wtQYAftHn/9MrfZ/wClH2erCWNr/wA8ZP8Atrsk/wDQHkrtl8BnSqwnW5NfufbvsdB4KF1qX2vT8jyQm/D/AJfL+dfVfw8v/wC0/CVkCMS2O63POf8AVn5B/wB8Fa+ZvA8X2bWCP+eyn/0Ja+gvhrMNN1nWtE4/gnj9fk+R/wCaV7PAvE39m5xz/wAyPlvEzIvrmTz/AJotP9P1PUq37lB4k+G/2b/lvpMn2aX/AK5x/d+//wABrIf7L/y8zf5/3K9D+Gx0W219NPuN/k6r+5lx8qnYGK5IAK9SOGHX1r+28kzTO8RL6xXpctKN/L89Wfw/nlXLcN9vml5a/wDA/E8B0nwdd6nc/ZtNtJLib0jQu35AV6/pXwbvIFF14huksVOP3YHmzD/gC/KfwavdNRiv9Jml0m1j+xQbuEi/d8dvu+1Vk0k3Netye2/eVpnw+J4wxM/4EFH8X/kjzv8AsfQtFH2fRbPz5/8Anre/vP8Axz7lZ0en/wBt+fomt/8ALx+8il/55yR/I/8A443/AI7XqEun21uM3NcLrus6H4b8rUNTvILGCJ/9ZcSJGn+7lyBn8a9qhToujKjTiebgM3rfWYV5zcpHnH/CGgcUf8IaK5nUPjz8I4r2aM+I4sq2Pltrhh+BEPNU/wDhfvwi/wChjj/8Bbn/AOM189/bWO8vvP1b6rQ/pH//0fz1n+AXh4/8es11B9Z42/8AaFZ11+zxGx/4l2tug6Ye1Vv+Bf69a+q0Ss258TeHIIPOTUYHMSb8RSI+fyJr9Nq5Rg4fGfM/2nXPkqX9n7xKw/0XU7WUg/8ALZZIx/44slcvL8C/H9uTMEtbk7N3yyMv4ZlWMZr69f4m+Gf+nif/ALZ1yV58T7m4E1vbWy+dv/dyH+GPj+Hu3Xvj29eDFUst/nO2lisT/IfKM3ws+IVtdm0k0kCcfIRHcW83P3sfuZX5xWZP4H8c23Fx4a1XjofsVxj8G2YP4V9VTeNPE32b7N+7g+z+X+9ijqPwn4n1208RabLZXKiczQtFJJHG7RsrhlKs6naQR1rwKtLDfYmz0KdWf2z411Gz1TTbn7PqVvLZH/nnPG0Z/JgDUUM/2mv03uv2rfij4W1GSxjubHWEhKkl1uI1kZcMpdbSe3X5Tn5VAU55Fed+J/j3/wAJb/yMngPw7fz/ALz97Lp6SSfvPm+R7z7Q6/P/AL9en/YVQz+v0z4NepK+p7/XvhBqX/Hz8N7aD/Wfvba8lt/L/wCAW32fd/wOuavNG+C9z/y561Yf9cpIZP8A0c8n+f8Ax/P+waxp9fpnz69fVnwguvHP/CNwacYY9J8OCS4ki1K5k8vzJJNrbUR/nlX733Eeub0i2+HvhvUjdaJD9tz5cv2jV9sn2f8A3Idmzd/tOn+4lcJr/wAQNZudak1W01K5v5y3/HzcIoBQE7VSEg4UDpuxnI+RCCD+Y8Se2xnPhoUvh+0+/kv1f3H6rwzSpZbCjmFeq/e+zC17eb/yPpFNJ8IWv/FSXMF74sh8zzPtMsn9k6X5e/8A5+JPn2/eX5HqjpHxx+H+m209xARoUqeZ5VtoNov2x/7m68uEk+WP+9v+evkrVtX17xFP5+tX1xqcoACfaJGcDH91T8qj2UAVFNpVzbW32nyf3FfOx4Ho1IcmLnzf16W+5I93H+KuJ5/9mgo/1935nqF/8ada/wCEl/tLT4ZL+3H+rj1q4a5k3/328ny/++UrnfGfxl+Jfj0TWviDVv8ARZmDNbwRxwIzDLDd5aqW68bicdq8y8uhIK+nw2S4ek7wpL+v66H5/j+K8ZiP49V/eEu+5/4+Z5bgRDEYkkaTaPQbicfhUX/bGh/tVtUttH9p/wCPavXPnfakUdvdXVzFbWsLuZm2oEBLk+iqOTXsfiXwL4f8K6+i6NqF3LcWcyrJa31pLa3yBbcSmV0dVVYmZgqjcXOCSCm2STy65+06XdQNbS+TPByrxv8AcP8AeBHRq7vw4dZ8Z+MrR724nvr293tLNNI8s0zrGTl3cszHAAyT2AzXXS+M48VVme4fGuw/4Q/xXY+KNNiSe7uFfck8aSRq0bBQx3A/MQwUegXiqeh/Gvw9rXl6d4otv7Ou8f6z+A/n939frXafHyX7NqVha7PN+aZ9n/bwn9K+Q9UsiLnIhxDcf8sq/MOHcko4zKsPPEx96K36rVn7Tm/E+Jy3OMRDCz92/vR6PRH0x4he603yPElt/p9jb/vIpYv+Wn8abP8AvmvbvDfxe13Tf2h/GNtbf6fY/YpNNtYr3zrny7eNFufkT92itHuleP5ERH/gSvl74eXl3qnw48R+HLrrpMcnl/8AXORGdP8Ax9W/76r0vwZPqVz+0hpVtps0ek/b445JZYo0j+0efZtMj/8AbT5Pl2bPlr6PhZSoutQl8UX+mn5ng+IdWGMhhsV/Mn+dn+KOo8Zwf2b4ynSDqME/9tED/wDs1dZawkePNOW2k8n+1T9nGD3kPyL+LbRVf4raVdad40ubbUeZ4Xj8xj3Plr8//A/v/wDAqzdb+0/2JY6lbf6+38uSKvms3z36nxD/AGhhfszbXpe6P2Xh7LPr+SQwVf7VJL/yWx7PbaV9mufs1z/yw/5ZV2tswyPJz5sBSWPH9+M7lP4GvkPxz+0VfXE+2PTRFLCCry3L5Df3WWNAMe/zGvAfEHxu8a6xiC21GSzjxjMBMZ/NcH9a/wBFn4tZVSoaz5vQ/wA98z4KxlafJycvr/wD9pviP8Y/ANlYad4g8QaxaWM9xCpaKeZUlIUZJWP77fgpr5D8U/tkaDb6jEfBjSapBAjhwUMCPJnjDOCwHHdPzr8ybeG6u7oZ8y4uJzgdXd3/AFJNeweI/hxqPgW00Q6r/wAfeqrcSSJ3Ty/L2qeePvH68gfdJPznDubzxHwUv3Ufn108jy6nAGEw0f31Xml939feeteMP2qPih4j85dLlg0S2OCBbRfvDj1klMjKfdCteJJo3jvxzqQuBFeavcXDbfPnZmZmwznMkh+YhVZsZztUkdOPVdDuvCFt4bmt9O025/4SK4t/K+02Vu9zJbyQQzu80LO/ytsb94u9Pub66f8AtLxvrWoz/ZbOz0n7BFJex/arj7TJHHJc6kqtD5Pyfu0uZYdrv/yyV/k+5XqZ7Utse5lmAo0l+5geIX/w48RaXcmzvre3Fwqoz/6QnWRQ/wDWqn/CEav/AM8Lf/wISvoC/tfifqt02sRCy1yLUlS7ju/7Jgl8yK5USxjMtvK67FYJsZ2Me3Zn5ap/2N8Uf+gRZf8AgktP/kOvjfaf1/SPftD+Q//S/PHxYNStp4v7RlN3Dk7X+b/Oa5eG+tv+eP8A5Er0i/vtC1vw3ff2lN5E+kSR/wDTT/Wbkf8A1f8AwGvF3ntf+e1e9Vq/vDL2R2ST2tz/AMtquvPa/Zv+viuAef7N/wAtq6mzurr/AKZ1wVRm15lr9mpdHf8A4ndh/wBfMX/oa1iOl1Vnw8//ABO7HMuP9Ji/Peu39aKQGbrD/wClZ/6ZQf8Aopa9M1Wf4Z/8TX7TD5H2+S4udN/d3MflxyJK9tC6fu9y/cX5P49r+Y6eZXm/iO3ubbU/Iud+fs9t1/69o69+sJ9ctvEk+paloMk/9r3sckv+meZ+8kT+10+xoifKvyq+zY/3dn36/V8N/Bh6HzlT+IeeQ6R8NLnw59p+2f8AE18uw/0X7YkcfmSbku/nf/d87/Yfan+xUWpeCvA39pQaZ4b1K41ae/vbe2tYoriL95byQxPLcu+zZEvnSbNj/P8AI33q6S5S51v/AIltt4VuZ9Vt47DUvNi8m5vPLjdUi85Efe3mQyP8v3/9T/10rsrPQP7N8beB/wDhJPCsnhOf7RfyebfRxW1v9o2RPC6P+8/dRzfPs2OkG/8A2N9eXnOPhhvcnPllL7PU66VLn98+PfGOi3OgCDT9U8y3v54re5EJjZSkcsauVffhgyMSjrt4ZSMnFZOleCdcv9Vgsbm0mt7iZ418poW8xlmQSKyI2C2UIZcfeBBBwQa/QXwf4S8DaJomlfEjxbZ28GuXF7/aUt98lt5fnvstk+eaNF/1yvtR4X3q3302V6BeeJfCP2j+zdE8SW0GufZ7iOWXTd/7ySNIEmf7SiW7/c2/657nen/fdfn+b46jGft8VPl9T1MLVn8EIH5WDQdbtI7+f7HILiGQW7fJxAAQWZ+PlZiNoz6N3Aru/iN8T7TxP4A8IeA4/DtnpTeFUuFuL2KPFxfvPJvDzv8AxeWuFT0XPrx7Fd/C/wCIq2Wo6fotlDeLfTB/lurdT8uMffdSOp44r5r8d+G/Emi34sdasHspmTeEJU5TcV3ZUkckHvXJSx+Aq0f9lxClLyaZp7Ktz+/DlPM/+nmuk8MaLqniXWtM8N6XGGvtVnjt4lkIRfMlcIm9zwq5Iy3YVWTTbq5/5Y122maP4jXU5L/R7Z4ZxlBIONgkQofm7ZUnPsfcVgaFH4ieDNe+Hni3U/AXiyH7FrOlTmCePcr7HQkMu5SRkEEcGuOSD/l5r3nU/g9rS3f2/wARa3b6hc6jGl7NJBK9w+64HmMJpHGTNknzASSGzkk8k0rwB4Z1K5/s3/SP9X/rfMoM/rVM8r8F6Pa6x4v0HRtSlC2V/qNpbXEhb7kU0yoz/grE/hX3L48+D/w9+F3xQ8EWHwuvLmea+8/zf9IS5k/e3NvbJs2fd8zzmr5c8VfC+2022+023mVr/s6GfR/ibpxgPkzwNG0J/uyRSeeD/wCQBXPj8V7GjOf8qb/A1wGFnWxNHkn1Xu97nr3x0f8A023/AOWX+i3X/ow14n45sbW5toNS86Pz/wDplXv3xZ0vz7vT3/57pdJ/3yyN/wCzV4VqVj/aXhuxuf8Ap3jk/wC2lHAuF+s5JRhDt+KbPqOP8V7HPq05+X5IX4M7bjxBrWjngX1j1H95HCj9HavTPDH9maJ8WvB3i3Upo/8AiYWVvFLFL/o32e4tIfsnzvM+z92n8b/xrXl3whb7J8V7Aj/lvHdJ/wCQXr2DTvjTqP8AwkfwyH7u3hsLe0jlMdunmeZP8lxdzbv4drK+3/VybW37/wCDy8DhuTGTnz/El+v6HVmeKh9QhT/lm/0PT/ir4k0nxh4km13Q5vOguYYOqPGd8Mao2VfB/h/Gr3h7TrbxJon9iedHBPcR/wDLWT/WfPWl8X7e3tPENl9mm8/zbRN2ERAG8x84VQMfLt7V4veabXwOPpUYZlz4qHNG/pc/bshxVaeTw+pT5Z292W9tfkmdlqnw807WdGi0nxTaJLPaBkjkRzvUcfdb04Hynj24r598S/BPVvDvnahpk39qWUI3iNEIuh7bQCH+owT/AHQOa9Z07WNbtp4rdpcib/np/wDFcmtt/E1rc3P9m3M3kT29ejkPE1bAT/ce9HsacQ8KYbMoc+Khyy/mWjPGPAGtaP4C0eXxVf2KvqXmvHZo3zMibUDM27GDlj0/hPHUg+f3nizWfGer/wBravwN3yAf59q+ntR8N6H4iMH9sQLMYJA4bHX/AGH/ALynuDXIePvB3h6w8NWuo+HpYzcR3yrJGh+YRyRs2/b/AHQVH/fX5f2F4d8fYbNYUaEKvsuXXkf2367M/knjngHH5XPnqUvac3210Xmt15lXS4PHFzovhy50Sz/f2AvvsV19oTzP3iT+SiIvzq0bwy+X/tsqf3N/R+IdN+INuL3UrbXrfz/7Kk1G2+zWaW32iO4mnuXhT/tj9pmjVPufc2J8lUvBOl6nqWiWOo22sSWH9kW2pyRfZo/3n+iNFc7Um/ePuklZG+RPuK1Yhg8M3OiT/wDCSal/xNbeT/ValcPJ+8gvFR9iI+zb9jjZdjwI/wB3Zv3ps/SuIvcqH5/lr1POfFGg+JrfVVPhHUpf7LntrO4TzdTi8zzLi2jlm3cDnzXftXP/ANk/Ev8A6CR/8GcVe3eDviV8KdB8PQaV4ig086hDJOZPN0uGaTDzO6bn8n5jsI5rqP8AhcXwO/599K/8E0P/AMZr4H6z5n0Kpvsf/9P82/D/AIVv777ZpxnhaS4tWYFZkI3o4ZUDZ+8Su3GR1OcCuN1DwnrejAf2hEYR23f07N+Br36w8d21tc/8TvR9O/66/Y0spP8AvtE3rXq15PodzbWNzbeZbweZ5csUsaXNv5kifcdH8x9v3W3Jv+7Xs0qR2VaX9/8Ar8D4Z+yXNb9s9z9m/wCWn+sj/wDIn363PGNzb2niLUVsESKE7WjjjxsThdyrgAbc5xXLW2uXX7j/AK6Rx1v9Vh8HOef7WfIdJcQXB863n/dA/rtq94aa007xLpV1dlpILW+guGJQSDbE6ufkbhunSuWuX/z/AKuo4NYubVoNRHE0Dh0/4CciuD7Yey54ch9u+LPCXw9+JPj/AFXxrrepXFhY3/8Ax7abZR4vPM3t99VSTyl/h+5/DXqUfijwN4TtoBa6F/x7yR+Vc6lcQ23l+X5r703v8vzyP9xP4q+FPHfif4ma1bT3PhvUrifSvL+0/wCjRv5flybv4kTeq7P92vHLb4YeP/GV95Giabd6y4Zl8xY2Zd27bjezBF/Fh+Vfl3FHD2cY+tOeMxrp0ukU3a3quW/zbPo+E6uW4DDQwWCw/NKK67/r+R+jOoftU6Vof+gadrHhzSoYP3ccdt9rv5PLj/6YwpAi/wC7Xknj/wCOngj4w+I/Dmmefc/6D9r829uLRLaO3ju0Vbl1b94/+pV/4P7teXeH/wBin4o6tLv8RXtp4fBA+R2aacHP91MIRj/poP8AH6F8M/sV+D9GBOo6jeapNNjcMrBF/wB8KC4/7+V+YZbDhDIsZDFRr3qx7K/42v8AifotTIc+zKjOh7Llj56f19x80eIPj3/wkdtfadqOjxz/AG+9t7yLzbxvLs7ex2/ZraFP7vy/vFd/I8x28tErntB8W6ZbalPc/wBmyTz+XJ+6ikT/AJbzK6bPk/uLsr9KvD/7Nfws0ZbZF8J2M8lvn5rtWum5/wCu7P8A0r2/R/B2haXbiDTtOt7f/rlCkf8A6CBXp4r6SGAw0+fCwnzetv8AP8SsL4I1p/x5r8/8j8zfCWneMPElzY22iaPqNhBcXPl/ar6N4/L/AIN/3Nm2q3j39nH4yS+Nb6ztbVdZS2maBbyOZVjmVCQrqrlZNrAbhlRwecHIH7CeHbAafNcNBHaxGdNvm3EC3AT3UMj4PuBmtTU9J8K6esRbxBJeQiP94Mtbon/AEx/6BXh4nxpxNeH1qhSXN5tbfem/Wx6dPwtwdOp7OpUl/wBu/wDDafefitbfsc/GO4YfbLaxtAR1kmZf/QVau+079ivxo/EviGysJP7ymWf/ANBjr9TNQ1f4d6a0NvpAa+lzHjCKh3/99P8A+y15p8UvjrY+CrGez8N6JaXN3BHh5Z7pnCTNgqQsY2soU7uGG7IwR1O2ScV8VZvUnDBTh7vxS6ffqr+hGa8OcPZdCFTFQl722v6afifJ2nfsWeIpQ4uvE8MflZYzpE0DtzxmNmZTx/darR/ZL8SWP/IE8Qx2/wDtSLu3f98qteea3+0V8R7rUZC+qmEqPk8hQi/Mc9VGfVeT+vNYlv8AE/xpcXPnwSS3MhRljhDM6o0mFVxGMgbGBIPQ9W3Cvv8AC5PxTOH7/GwjLyin+dtvQ+GxWZcPfYwjl6yt/mdlqf7MnxQtbeUpqthqg7RRSSJJ/wCRAij/AL6pngz4C6r4C1KXxVr8UUFyqMIY4n8za7ZTc7YAOUYgquACeHYZFeu3fiXWh8t1Khn8uMyeWeEkkjV5I+c8xsWX8K5K88Qanc/8fM3/AB8V+T5jxlnso1cvxVVbtcyVv6XyP2nI+AMnozo42hDzjr933HEeKdEHiO1AN2LKS3fzPMfAQDb8+49l6HI6Yrwa28MW9x5Giad4v0e//wCeXlSeZJ/f+5v/ALlfQU7/AGj/AJ5/6RXzP/wrC58N+Lf7S03WJLCCe58v91Gslx+/fZ8m/wCT7jfx/wB2vuuBc8xEIfVYYjl/l0X+V+x85x/w5hp1oY2eH5v5tXe33+p6H4N+Gen+DfEX/CV6lqH2y9zIUjjj8tE3jH95s8EjpXL6j4f0228feFdN8mOf7fcfZv3sf+rt5NybPkeP5Y9y/wAddfqHh61t7uGDUdR1C9nuf3uJLx/4Nu/57f7On8X3dlYWsWOm6JqWlalptnHB/pP72X/lpJ91/nf+L7terSzOtRxn7+rzS9Fb+vkL/V6jicHOhQpKPzd7/wBeZJa3LyafaWDszQ2alLdWJPkxl2k8tSxJ27nY/jXrP/COf2lbfabby/8ASI/MrzPVYPs2t31r/wA+800X/fDsP6Vtw6xrumW0H2ab9xXz+ac86394/UcspUaNGEKEPdDWNG/s3yLn/n3kjrn9Y0P7Tc/afJroJvE9zqVtPbXMMc/7ur39saZbf8hKH/j4/eebWWAq1qMzvxVKjOjA4TyLrTdN/wCJb/r7eT/yHQmuf6N9p1Kz8jyP+Wvz120MGmal5/779x5ccn/slR/2Pbfv7b/n4jk8r/lp+8jr1cBj5+5Cf2mZ4/DUYc9SH2Ueb63pmma3bxrvYwc8xS78Of4hu3Lu98Vz6eGPDNv/AMw6Sf8A667/AP2R40r3H4WeD/Deo+K9F8PeI52sbG+uiJJd20cIWRN3Vd7qqcc/Nxg11PhuO003/RvGug6jpOqW8kf2mK2t5rK3t/8ASW3wrf8A7t5Fji2fM8773aT/AGNv9H8KZxmWMyqE/rD5Y3Xnv3/LU/B+K8Tw3lWL5MVl/NVlr2X529dDwIeGjj9zoduU7YtkYfn5HNL/AMI1N/0Arf8A8BE/+MV7L/wklr/y9eBtP1if/lrdXesaZ5skn8f/AC/fcVsrH/sBaP8AhJNO/wCiZ6N/4ONM/wDk6un6jW/6CJ/ieL/r7gP+hVD/AMB/4B//1Pzxt7m01O/0yDXdRa+sRJ+9SHzN4Qkbtm9U69qyZvFn2fTxDp0ph+fIOcP8uP7tUZtV0P8A58//ACJWI/8AwjP/AD5yf9/K+q+oYj+Qft4fzmRdap59zLcTyebLNu3Y4/ktZP8AaNt/1w/z/uV1MWlafqNx9n0qzurmb+4gMrn6Kig/kK9M0T9n/X9aOLqybTBkjN3Mqtkf7C7mH4gV4Gb4+jlsPaY2aj6tfle/4HVhcLUxX8CHMeOaCw1r/RhL5txO+2OERvJI/wDuhFavofw5+zJ4s17bJrFymi202G7mYqeq7eAD+NbED+GPgnot9puja9Z6Trn2mSKWW5t2kkuI4/vv8vzrFv8Akj2I/wB1qTRv2lfA1lpv/E+1K6v9S8x/3lpAwhZP4flldCD9RX5jmfEGcYzn/siCjH+Zp6+aurfifeZPlmSUeT+0ZuUv5VsvXq/Ox9D6dc/C74UadaeFtT1vM9hCmUSNnc7ix3sqKcZJJr0jwv468F+IhP8A2Is5hsI0klICIFTcEJCby7KGK/dQ9a+Grz9oDwN4kuvtP/CLapq0/l+V+6s1j8yOP/t5k/8AQKjPxp1vTf8ASdF+HGowf9NbiT7N/wC2n/s9fD4/wzx+Po+3xs5yq93KNk/K1z7LC8fYDB1uTC8kaXlGV383Y/RG08T+CtL5t7QW/wD1zjx/6ADXZat428N6dDEfONz8uQtujy5+nlht1flc/wC0R8VNE8+503R9C0me48uOX7VqCSSfu939+5j/AL1adz8fvjP/AGZY6lc+NvC0Bv8A/l2tf9JuI/n2fvo08zb/AOPUsB9H3ATo/wC21Zyn5P8AzOHM/GXE+2/2WiuXzP1F0zxlo9x5VyisxlODH/qGHvlgBj8ap638S9Gtx5GnyeeSOOqf99cV8c/DD4ma14+8Gz3Ws3MF3d2121tvgWRVbZGH+dWG3dhx9049h0rrdVS1HkG1l86GeGOQ/wCw7D50/wCAsDX5Nxh4b4PKsZ7CHvQ0eu5+s8HcQzzXDQrzhyy1Xloeoap8U9SHFqK5y8+IfiXUrQaZ+6itxnhYE38/7W3d/wCPVw2nWF1qNz9mtjX1R4G+CGjS2b6rq2s20QjPKPuDt06DHP4Zrx8DhPY+5hvzt+bR9Pi61Cn79f8AK/6M8BtvD/iPWfI/0Sb/AEiPzI938cYz8y+o4P5V1Nr8EbjU/I/4SP7P5FvH5cX2mzhuZI/9nf8Af/77evpfS9OtCv8AZ+mxY/651d1UJ4Xmmh8QGKzkhj810kPzpH8qeYy/w/Oy/wDfVeVlHE2cc855ZB/K7++2h52dxwVSPs8ZGPzt+p4BZ/ALQ+vkx/6P/wBOdt/7Olb5+F1sTvgnuITnd5ccjJGfqq4z+Ne9eHPiL4f8QNcaf/ayOmmxpKYoYgoVmXeSG5z8v3gHYLg5I5J47VfE4nJufO4P/LSvQ4tzPiHCwhPFYir73fT10uzwOHcPl9ac/q1GHu9rP9DwrVfhPa/8w2byP+uteL+L/C2teHcm/i4Bwr/w/MuVr6R1XxP9pufs9tXyf8W/jBqngoxQfZmWKXLRrO7FXVuBII1YMqgnhm+9kgAbSa9Tw3yvMMfz+5zcvp+L2sexxhnkMBCFSvNR5vW+3S3U5+we5ubaf7T/AM84/wD2asDWIDc3Ol3A6W9wJXB/i2owA/MivE/Cvxetjrl99m03yLL7NJcyxW37uO3jg+d3RJn+Vf8AZT/gFe7fbrbWtFg1HRZvPguP3scsVfrVbJq2AxkKlaHLH8PwPg8LmlHH4OdOjPml+P42OeuftNzbQfuf+Pe9kk/1fl/u50b5P++9leS+EtRufH/2621K8/48L391+7T938kvyf8Ajtev3M9t9pgtrb/nnH/y0f8A9n+61eJ+ENDuNE8JXtzrVn+/v9R8yOKWP/nn8iP8/wDt7q+yoqlCV6/xe7+b/Q8b2uJnyToe78X5JdbdT1XxHCLfxFfkfxXEkv8A38Yv/wCzVrpqVtbW0H77/lnXNXV3candm6uf9dPjO3/YQIP0UV6LoOh+EPEdt/pFn9nvv+Wv7z93J/tpvr5HM6sOecz9gyylOFGEPI5qae2ubaf/AFdZs0dtc20H2n/nnXqNz8PfDn/LvI0R6f65f8DXJQ+FbW4uf7N1KbyPs8fmebFInl1wYXH0ef8AdzPSxWFn7HkOWeC2/wCPb/lhcR1k20H2a2/0ab/npXd6p4WttOtyILyOU5/1bOof/gI4JqreeFf7Ntvs1tqUc/8A0y8xPM/9DrvwuKhyfM5sVS9/5HA6pfj+zhc6lOR90+ZGjn05+Rcj8q4FvE/hkEEXkn/bPd5n/j8Mf/odd94y0o2vha73XKykr/CyjvXyz5lf0F4bZZhsZgJzr9/0R+LeIniHmuT4mjQwU/dt66nrv/Cb6B6X/wD37T/45R/wm+gel/8A9+0/+OV5F5lHmV91/qpgP5D8t/4i/wAQf9Bf4R/yP//V/KS5/wCPmpbWw/tPUZ7eDOLe3uLg4/uW8TSv/wCOqayXu/8AX10HheX7V/wlU1s3k+Tod23zcgiUx27KB/ESJvu9xn0r9WxOK5ITmeGXfD3ju5tv9GttN8iD/lrF5k0fmSfwb5kh+792vQ/gTc6Z47+Jr+FLnwxa3x1WBoo0RmaRmDqxWN528tJHUFRI6lY87/4Qa+fNH/0a5/59/wB5/wAsre5/9ovXf/Cyz8Rf2/qmpaLE0d00KQxM/mLGW+0Qu+Wbj7iMMe9fBUqtb+v6/M9SpShP+Idd8ctG/s3UoLa2025gnsLf7NdeVZpcxxyR3Nynk73/AN1f8vXi9n/af2b/AF2vf9utv5f/AKC+yvWPjFHqg1ma2tvtl/AE8sSx38dvHJsmk+fydnLfNu3f7X+zXjVtpX2m2/0nTZJ/+uuqQx/+hxfNXzOFqwrQhPyR6mKpcladPzNc2uu3X+jf2b4lvoP+nm8eKP8A74eGSk/4Ri1tv9JufCv/AIE6pD/8RHWTaaVoXbQbL/trrlp/6B+7etZ7W1/5dtH0H/wcf/EX1eicZW8i2+0/6Npvh2D/AK66h5n/ALd16jon/Ey0SfTbm88C6F9gtpP3t7b+ZcXHz+d8kiQ3DtN8u2P/AGG2fcrylIP9J/1Ph3/trePJH/6VyV6b4H1G+sdegjtrnwNaeZOn7zWbJL60jYZwzxyQXJ2887Y2PfHHG9ID6H+C7Y8Es/2qC8+0X102bSBLeIbEhXYESKIZGN27b8wI54r2ywsbnUvItv8AyLXh/wAKr0y6KUN1aXn76Zi+nxeRb5Zh9xPKhxwB/AK+nvCU11p5+0af/re/ps/2q/kzxXxX/CpP0X5I/qzwup8mSUfn+bI9f8VeHPhfpTXphe9upz+7hiK+Y/zcs7MRtQc/MQxwNqq20hfObT9pLxLrlxPa6dBkXEflxyW8aSfZ7iT7m9neRN3yt/H9z59lZXxU07xN4k8fz/23DZTz/vJIrmy8n7PJZ/c2Wz23yStI/wAnyb9m2bZ9/wAuux8Kp4Z03w39p8N6PHpM9xe3EkVr5jyfZ/3LWyI7v8/3287c+z/fSv1vhnwgy3Bx9pjYKrW89vktj8f4r8Ucfia3s8LP2UfLf1vv9x1Pg/UvE1zqVjdeJPFVzb/Z7n97LYyJ+7k2Ls+/9of+H+DYnzVJ451X4Z63ps/jbxbDqPiXVb+SO5/eyTXMf9nxv5M293+62/8AjdP4K6TwxrnxCudE/sTxJ9n/AOEc8v8AdRS+dHHJcSO3zpMk0m3/AFbfMj73+5XW6l4cudS/sq5tvLt9K/4H5clvG/zo/mJ8sVxtX/x3959+Ov1P6ryUeTCw/Rf8A/K/r/PW58TM+PvE723hK2sfFvwlhuIP3nlXVrbRvZeZHO8UP3/9cqyPJ5O3fsf/AND9x0zxVc3Om/6R+4nt/Ljl+5/rJIYpnbYnyL88n3E+5trD+Jes6HqVzY6bcmO/0r7RaXN9Fpu/zLyS03PFGm9I3tbeOaRpt38bqvyJs8x+GXUv7A0O+1nWJ/l3z3j56Lxwo7nCKqj6V+Q+LOJhPLvqVf3qsmuWJ+x+EuVz+vfXYe7Ss+aX/BN7xT4zt9Gjknvph5Fs0TyRAkPcMxLrEGAO1dqMGbtuXHPB/NH4g+Or/wAeeK73xFq4x58hIRCdqISSqqpJO1c9/qeSSe7+I3xI1DUNFOmzzgzX8nnyY6RxhyUjB9flUk9cDnhuPAYX/wCXavrOHsihlWAhgof9vevX/I+Yz3PJ5ljp4r7P2fQvJXsfwK8Wf2Nrs3g7UJSbO83SQDPCTZAxj/aX9QB358Jef/UXNX7q4udMvLLWrf8A11lKkkf+/GQyn8CK9DHYGGJozoVPtGGAx1TDVvrVP7P5dT9Ab+7/ANI/0avOviLrltbaJ9p1L9/+8jjq7/wmOgT2sVzc6nawGVFcxPMgcbug2k5z+FeY/Ebxp4auNLigsLmO9llnTMaOHwu1ju4z0YLX4zkWSVPrML03ufvmb59To4Oc6c1t7pPpPxEFhpF5cfNDBbpn5ny7nP3env19/aqUfxZtNYgFuNNaL7vmF59g9/4DXh/izVtEmJ03REjihhOZJdo3zP8A7J/u1U0jxDPb289v5X72ZNm/0/3a/U/9XaMoc/IfkVLxEx0J/u5/9vWR7vq3x51hLax8Pw2EM1ppYdIJOd7h+u/JIP5VzWpfGfxXrlzZ3V5bQGazhMMZVWG1Dk7eWPdj+deXW1rXf6J4f+03P/LOuqrTw1H4zqwGOx9afuT79uu/Q7W217xvrcEE/wBrgsrHSbPeJHt/3dukjszJ3/jrDHjLUP7R+0z+L5Z/3flf6PaeXJ5cabF+dv8AnnXa+OFbR/hDNb+cnm6rPBHjH8Cnzf8A2Svmi2j/ANG+0is8jq/WYTqedunT1udPGGJ+oThQn8Vry1fVvTRr1PQL/XdSxALbUbiey8uSPy5Y1/1n3Puf7jVy28VRs777TbT21z/z0/z/AOy1LX67w77mGPyDPMV7at8ZYooor3vanjn/1vx4d69d+HmqeF9F8K61/auoWEOqavHH9ni1GNvI/dv/AMtdufl/i+T/AGa8Zesjef3/AP2zr6rirLfreH+rc7jtt63IyzFexrc/IfR9t4iQf6m/8C/98agP6V7f4c+IHg8+Cb/SJNb0j/hJr2QRRx6VG8fybl+RTN8/Kq29v9qvjxLZrbTNV07+15RrkEf/AB7eZNH9n8uRd/8AwLb/AMA+auc8C67qV14q0tbq8nmV5RE+ZH5LjYm7qT8xXg+lfj+bcDxmrTq35ddD7TK8/tOEPZo+vfiJ4HHjbw7Y3Gjabbza3BLcRSy3G/y5LeTa8S/I/wAvlvv/AIP468Rh+DXjm2/0Y6Dpc/7v/npd+X/44/3q+vtH0k3WmZ877BP/ANc0/wDiK8w8IL8YDrUNr4ocm13sjvHHZBSvJViy4kxnHv6ivznJuN8ZChOEJw/dfzaP/gn6lnnA2DniYTnCf719FdL100PG7b4NeOLY8aDo3/lU/wDi6n/4VP45/wChc0b/AMqP/wAXXqesap8aNN1u+traGSext5JPKlis/M8yP+D54UrOTxf8YP8Anzf/AMFl7/8AGq+wo8R5vUh7SnKj97PkavDOTQn7Oftfu/4B5s/ws+IQuPP/AOEW0fH/AF73P8f93f8A+z10ul+F/idpdrcWq+C/DU/2jgyXGn21zIn8P7l5vu16h4o1T4l6J4c0XUbWHz5r7y/tH7iby4/MVn+799f+B1F4e1L4l6lqUFvcw2Xkf8tf3f8Aq4/+/wBWWG4yziVGeJhClyxv36fM66vBeSQxMMNOc+aVui6/I6n4daBcaNo3k38Fvb3lxIZJIreNIo48gKAiphcbVDNgcsSehFeqtfc1gWy0PBg1/PmcY+ePxM69f4pH9BZZlkMHhoYWh8MTq4fFmu2owNQm/wBYkx+dv9YmdrZz94bjz15NZM9y915/2meSb7Q++T52+d/VvU1i+Ta0fZbf/nlH/wB+676nEePnviJ/ezlpcPYCHwYeH3I0vMtP3/P+v/dy/vH/ANXWReXNsf8Aj58v9x/z1qzstv8AnjH/AN+0qNMisquaYif8Sq5erO6lldGHwUlH+vkUl1W3H/HveR/9s68p+KWuf8Sw6X5nkxAJNcOeAI9+EXqNzM3T/dr2N7vjNzXwX8T/ABYNY1KdraTmYnn1RT8uf+A1954aZPHEY/2/J/D1+fQ/P/FHOJ4bAew5/eq6fLr/AJHl1/d/2lcz3NXtO8JeJNRIW2tBPuOOJE/8eyw2/jXSeDdA/tCeGc/8t5BGntjl2/Ba6jxrqC2GPC2nTBrSHPn45zv/AIW/A/rX9C+yP5jpYo8cv9NubT/R7kATdCA6P+qk/wA617mD7Ton/bOs25ntf+Xauk0f/SdN/wDIVY4p8nvnrZZ++54HndnPa23/AB82cc9dBfw6dc20LW1pHB+7r1n4G+F/COuT62vim0WaayEIj3ltpEm/d8vQ42jrnrXoni27+Bttc/8AEyhsv9Hj8ryraSWOT/vi2f73+/XefNnxNXQ6Vdi3UpdKZ7c5fy9+z95sKq3Hp/Krviufwzca1Lc+ErR7LS8R+XHISzj5BvyWZyfmzj5umKxYetBoaOpT3X/PaSq1pPqdz/y+S/jI9e9eHdBsfH3hrSvCsl5b6JPo63c815ccfaDcum1MqD9xV4455/H0DVv2cPDHhvw5feJNM+KelatfWFvJcixjtLmOS4k/uJJu/wDH6qqa0qszwDwpoOseLdRi0Q3bBRHJL8+5gAo647ZbaufcVZ8OaKl9r2m+HrtB+9utkiZ6+Xkuu7twpr179mnTJfEHxVj0yA4mnsZj/F0DKP4uO4rXtfD9ron7Xln4bu/9R/wkscQ/7e/uf+PSLWHtSKp88axov/CP+I9T0cHIspig+h5T81INVdlfW3xX+H2oXv7Qfjq1tUJs7Wa2jDFdysy20TMoZflHLH8evOa55/hdqf8Azxt/+/af/GZK+zyvFQhR5JzOSrSnznzVso2V9Pf8K01f/oH23/fR/wDjdH/CtNX/AOgfbf8AfR/+N12+1ofzfl/mcnvn/9f87U8F/Be5/wCPbUtVn/65Xmm/+gQpcVWh8I+D7bUv+JJDez/vI5IvNjmk8uSP++6WMaf+P19KJqX2b/ljWTf6/wDZv+W0dvB/01kr0K2aVp/GZfVTwmXwvi2ltlzCZk2eZlmf8GfO2sHwx4B0Lwn420P7TDJP9okk8rzJP+WkCb0/g+9/8TXsV/8AELwzbf8AHzqUdfPvn6H/AMJ/pWt6JqUnkfbf+PWXf5cfn/un2f3fvV59Ln5zU+otW+CPhTxrdTaxqOt31pdzkHFoixomFxsAdpSRx1zWEP2ZLOMk6R8StStiePngIOPT5Zga7O/8UweFNHlv5oXnhh++E+9/SuPh/aR+GuBBfTXVlIf+esDH/wBF+ZW9TD0ZfGc2FxFal/AnylmT9mj4hk/8S74vN/20a6H/ALNTf+Gdv2g7f/kG/FKxn/663E0f/oa12Vj8cvhfMu8+IYYRnH7zdF/6GFFdtp3xF8DakC2n67YTgf8APO4Q/wAmNcv9iYOf8SmvuR3riDMqf/MRP72eH/8ACjv2urb/AI9vFukz/wDb5b/+zxUj/CL9tbP+iC3v1/6Z3FjJ/wCypX01balplz/x7Txz/wDXN1P8q1krnq8L5dP/AJh4/wDgKNqXF+cQ/wCYuf3v/M+OPEnhn9tbwlbfadS8Nm4g/wCna3huP/ROatWfhL9t7UraK503w200Fx0k8u2T/wBDevtiz1K7tuba7kg/65uw/kRWSl9c/v8A99J/rJP+Wn+3XL/qdlf/AEDx+5Hd/r9nH/QQ/vf+Z8mx/C/9uq566BBD/wBdJLEf+z1pp8Cf26rg/wCkS6dZf9dZ9O/+JNfT32u6/wCe1RzXda0eEcrX/MPH/wABX+Ry1eNM4n/zEP72fOzfsw/tgCR477xloFmEJUeZfQHOOP8AlnE459ifqasf8MkfGu9jZvFHxl0q0ZQSqwTXEuTjoBHGo59yPfFe1vPUX2iu+lw/gYf8ul9yOZ8TZl/0ET+8+afH/wCybrXhvwbqXiSH4xW+s3VnbS3ItBDcxCZIUMjKkrSFQdo4DAAnuByPhLTbW41G5gtrc4lnfaP/AGY/gOfwr9Sfitqhg+GHiEAgfabOa2OfS4Hlf+zV+dng6wtftVvOYvPMKu/+4eAv5gtVvDxpfBAx+vVq38efMex+GI7XTft2pfvILHSI/Lii/wBZ/q03v/383L/3zXgOsXf2n/SLmb9/P5ksstex6xP/AGb8Lv8AsLyf+j5PN+//ALlXv2XvhpP8X/jBaeG2wLe3gmvJR3KQ7Y0C8jnzZEZh/cDYwcVhSH7U8l1T4YePNI0NfEt/o0kNgybwS8fmKrcFnhDeahB4+ZFrI8MT/wCk/Zv+2lfsj4u/ZstfgZotjpus+NI/FV7qEkkd19qj+zySRz7nTeryybpdu/zPn+f+58lfj74m0j/hBfH2raLP/qtMuHVev+pf54yc9yjKT9ajFUueE4HflmK5K0JieDPDVp4j8VXWi3W/yvKaQIjbd21gPm7/AMXbFe2+I/h3omh+DdY+w2aRyLB8hAzJ+75++cse/BNeffCA2998TZRbyfup7aYbv+Bo39K+tdU1rw74WgElxcpDMeAxYDPtuLV1U/gPOxXx/uz81k03/oJTeRVmG0tv+Xmb/v1X0z4k/aOlUTaf4chedvu+bOMRfUKDuI+pU18sJPV/uwJXk+01FYPUbdarQvWQH2f+xX4u0jwj+0F4futfkWGy1OK6sPNc7VjeVN8e5sH70kaIP96uA+OPjCK+/aD8T+MvDDhFj1t5rKQHeMWkm2B1J4b5URh2rwWF69f0H4c22peRc6l5kFZge6+GPiXrGsfb/EGrwxSalrl5NeXEkaAANIFUoiDhVCoFAHbvXocPjG6uf+en/bKSuE03Q/7NtoLa2ru9K0D/AJebmtfawOv2Ra/t+6/6af8Afyj+37r/AKaf9/KsfYbX0lo+w2vpLT9qYH//0PjCbwPrH/P0v/j3+FUrz4ZXVz/x8/v/APtpXvzyVHm2/wCeNZe1A+Rr74U6mxHyn/vpRXNN8KdR+0/arbjyX3x/8B5FfdVzY/2lbfabaH/rrWA+h3X/ADxjrOriphSpHJNDb63ps2jX8pgjvoHRsfeQuMbh05UkEe9fFmofAH4qWhkaDR/tiNj95BPE/J/2N4k/8dr7T8f+H57nwpqflSEyQR+YoQfMdnzn5l+lfFY+IPizTTmG/fowXeFf/aX7wPr+laUqvOZeyOB1XwL8QdNO3WfDuoWwHO6a1lVRj/aZQK4qYW/2jI6V9FaX+0r4+08/cilX0IZf5MK7a0/aji1Bx/wkHh6K+UDAE0cc4H/AZFIrUfvnyHbH/n3m8ivXvBHjjXtA8W+H57DUbph9pgSeN5nZGVnVXXaxxt2/ljjGOPdv+Fo/s961/wAhrwHp0H/XKze2/wDSZ6t2Mf7LE1xZX+ng6fcW0sdxHm/m8sGN933Zq0H7aX8h9xJPVZJP9Jn/AOun/si155bfEvwh/wBBe3/7+p/8VWla+K9DuriX7NeQT4RfuSIeufQ+1dhweyO231WeSsX+3NM/57x1VfWNM/57R0GJrO9Ut9Yk2v6Z/wA/kf8A38rEuvHPhG1H+k6lbw/9tU/xpe1NfZVDzL9pXWlt/An9knP/ABNriNCP9iE+bn/vpFr5dsP9G+3f6z/R7L/llv8A9quu+NvjbT/GXiS2t9ImN3Y6VCU3A/I80zKzn8AFUe+e2K43f/o2q/8ALv8A6P8A6r5P3nyN/wCjPuVw1P4h3UqXJA6P4jzfZ9F0vTc9Dkf9sk2f+zV9c/8ABMbU7XTPivrEVwhL6raBo2/uLbPukbr0LOoPuR+Hxh8RZv8AR7H/ALaf+y19qf8ABPxtH0628Qa/cQA6jbzR2sUv9y3uVDSL9GaFT/wGsaRVX4Dy/wDaH+I8fxM/aM1iTUbBdPNvNLpjtqDs0Ksk7ra3bKVCiNlKPtYEKpJ5rw39pXSL/T/HsbajcwXt/f6Za3E1zaJ5cM7jdCHVd7/wxgZ3c4/Gq3xV0zxVdfF7xBYXy+dqUyzXM7wcq8cEHmMwUZ4WJDn/AHe/fZ/aMsvBNj4y0m28CaPPoelR6RZKILqCW0n85i7SO8c4DlmBU7vusCCvqdSz520LVbnRrqLUrHmaHOzPuCp/QmtK8u9S1qfz9QunmlHA39v92oraD/n1hrbto/8Apj589AGImm1pWeh3Vzc/ZraGvTdH8HXVz/pOpf6ivUdH0e103/j2hjg/6ay1y1cUb+yPILHwKQczFSc5Hy/L/MV0lt4S0P8A5ebO3/7916s9pdXP/Ht5daVhpV1bf6Tc1y/WmdHsjhNL8GabCftP2WOI9M7Rv/OvSIdK+zVt2z/6T9mrv9Vsbb7TB+5/f+XWXtTUzbmwtrbyLnyf3/l1G99WjeR2v7j7TWc6VvSMyjv/AOulG/8A66VY/wBG/wA7KP8ARv8AOyuoD//R8d8y6pP9J/6aXFS7/wDpt/39q7/o3/Pb/v1XFVNAtru6trn/AFP/AH9rfdPtP/HtWTDd2tXv7Ytf+XaaOCsAIk026uf+edfnt8ZPhvdeBNcZ7ZWOmXzmSCXspJyY244KnOPVSDnOQPtd/Ef2m5ntq4rxXr+mf2ZNp+pwx3EEwO+ORPMDj/dwc/lRS9wdWkfmRqUH2a5/6YVmV6/41stA+0bvDtnLbxZ+dHk3xgf7KkFh9Sx+gry+bTrn/njXfSMii712Pg2y0+fUYrrV8fZLf52Q/wDLbH8PUda5dLWtJHua1A+ndV+MVtqX+jW2mxz/APXWvM9e8R2mo/8AMNt7f/r3DRj8lOK5LTaLmOuH4Dsl8BG0uP8Al8uIP+3hv/i6ovBc/wDQSk/7+PV7ZRM9dQvZGRNptzUT2Na6T1WuZ6XtTGrSLOlJ/o0/2mu2f/mK21t/z7f/ABVcTok/+kz23/PxHXU2b/abmD7T+/8AtFv5X/fv/wDarUyNLxg/2nTYLmvY/wBmL4p6P8Oz4hTxBN5FnqRsgrbdxV4mlUs3cLiQdMnjpXh1+/2nw3B/z3/9qR/I9ZvhDV7XRtZh1C8tUvYIHUyQSIJEde3DDHBww9x+BzA/RrTfgjqXjT4yQHwVq0Xh3+3xJc3N8c3F5HJvlllW2Rv3O112rXx3+1D4qXxt8efHGurILsfbxYCcAYcabEliHG0lSHEO8HP8X419cav+018M/BPgq+1vwDqUmreOL+3+zWP+jzW0el+Ymzzn3psby9zfufn+f/Y+evzf0q0+0/8ALGtDKl7Qj0q7122/49pq7/StR1P/AKZ/9dZasWfhi5uf+WMdek6V4O/6Y1y1asDq/eF3TY8f8tq7uz0b7TWZZ6HbW3/LHyK7KwsbX/ntXBVOsIdN+zUvk/8ATc1tJB/0x/cVt6V4c+03P2n/AFH/AGzqADRLG1tv9J+x/wDf3f8A+yV0F5Pa/wDPH/v1WjNY/wDPt+/qu8FAFGG1trn/AJc/s/8A11qtNY23/PatJ3tf+e1S7v8ARv8AXR1dI0M3+xrX1o/sa19aveXdUeXdVqZn/9Lw957WsibxHa23/TeuXv7W6uf9GqKGCvPOukaVz4qubms2HWP+XmqTwXNRJH/y7edXOL2sDEv/ABHdfafs1t+4rgPEmsf6N9mr0PVdHtbn/lj+/rgdY0r/ALYVtSIqnkF5Y/8ALzWI9pXqz6VWJNo9dPtTA8um07/pjVb+zvavUbnTaovB/wBMa1A4mwsas3kf2aup/s2196kfRrWsatI1pVTzd3rNuUua799A/wA+XVKbwxdf88a2MjgKjeu1fw//ANdKrf2HQByVtP8AZrn7TXbJdf8AkCT7TF/1zk+//wCzVkzaNVVEubagDtof+X7Tf+2kX/bT/wCz3VxXmVJDqv2b/t3/AM7KLye2uf8ASbb/AL9UASr1r374a+Dxc6bBf3EWfOO/1+X+H/GuS8B/Dq51i6i1HV4vKtAchD/H/wDY/wCfevsGwb7NbfZrby/I8uscVVNaVIybbQLaD/ljWlDY/Zv+uFbem6bdf8vM3n11D6dXCbnnk2lUW2lfaf8AltXR/ZK6G2sbW2/4+azNDIs/D9z/AM9q6hJ7q2/0at9LX/l5tqXy7b/j5oMaVU5+GCrPkVpfav8Al28mo5rH7T/y2/z/AL9c51GJN9l/5eYasW13pn/LtD5E9SpY/wDberML21elSMqou65/57R0brn/AJ7R1F9kX/nvHR9kX/nvHXT7Iz9qf//T+X6x7z/lhWxWPef8sK5wKN/VK0/4+au39UrT/j5rjNDSm61WT/j2uKszdarJ/wAe1xVUvtgeb6t/x81Rmq9q3/HzVGaqMzEm6VWqzN0qtQBXqs3WrNVm610AbUPSn0yHpT6DMo3iJ/dH5VjXNbl3WHc1lSFSKVZE3StesibpWlI1MCevUPAFhY7hJ9mj3+uwZ6+uK8vnr1vwB0H+e9PFfAB9C6ZXQad/x8/9tK5/TK6DTv8Aj5/7aV5Z2HQTdIK0YP8A2rWdN0grRg/9q1l/y8NDStv+Pmultu1c1bf8fNdLbdqg88b/AMvNTf8ALxUP/LzU3/LxW9U9A1qyf+Xitasn/l4rUCPUP+PmChulGof8fMFDdK2pCqlWiiiu45D/2Q=="
        main_data = {
            "vehicleEventId": "9890bdbd-0fb5-426c-adf6-cf84a8bc0f55",
            "vehicleRegistration": "abc-124",
            "vehicleTimestamp": str(datetime.utcnow().isoformat()),
            "entranceId": "mcc-Alt-01",
            "vehicleResult": "",
            "vehicleImageBase64": vehicleImageBase64 ,
            "wheels": wheels,
            "secret" : "daddbb60-89d7-4ea7-ad93-7b6966645640"
            }
        while cap.isOpened():
            success, frame = cap.read()
            count=count+1
            
            if success:
                results = model(frame, conf = 0.50)
                wheel_bounding_box=wheel_detection_points(frame,results,model)
                if wheel_bounding_box:
                    print("count",count)
                    
                    xmin, ymin, xmax, ymax = wheel_bounding_box[0]
                    height,width,my_dict=prediction_result(frame,results,model,model_seg)
                    target_folder=classify(my_dict)
                    image_with_text=write_text(height,width,frame,target_folder)
                    
                    # annotated_frame,df=track_wheel(model,image_with_text,target_folder,c,current_year,current_month,current_day,df,frame_height,frame_width)
                    resultss = model.track(frame, persist=True,conf = 0.70)
                    # Dynamic AOI definition based on the image dimensions
                    aoi_width = 0.95 * frame_width  # AOI width as 40% of the image width
                    aoi_height = 0.9 * frame_height  # AOI height as 60% of the image height

                    # Calculate AOI coordinates
                    aoi_xmin = (frame_width - aoi_width) / 2
                    aoi_ymin = (frame_height - aoi_height) / 2
                    aoi_xmax = aoi_xmin + aoi_width
                    aoi_ymax = aoi_ymin + aoi_height
                    frame = cv2.rectangle(image_with_text, (int(aoi_xmin), int(aoi_ymin)), (int(aoi_xmax), int(aoi_ymax)), (0, 255, 255), 10)
                    if float(xmin) > aoi_xmin and float(xmax) > aoi_ymin and float(ymin) < aoi_ymax and float(ymax) < aoi_ymax:
                        # frame = cv2.rectangle(frame, (int(xmin), int(ymin)), (int(xmax), int(ymax)), (0, 255, 0), 10)
                        resultss = model.track(frame, persist=True,conf = 0.70)
                        frame = resultss[0].plot()
                        track_ids = None
                        if resultss and resultss[0].boxes.id is not None:
                            track_ids = resultss[0].boxes.id.int().cpu().tolist()
                            # Rest of your code using track_ids
                        else:
                            # Handle the case where results or results[0].boxes.id is None
                            print("Results are empty or results[0].boxes.id is None.")
                        current_time = c.strftime('%H:%M:%S')
                        
                        if track_ids and target_folder != '-':
                            
                            for id in track_ids:
                                id_list = [d['wheelEventId'] for d in wheels if 'wheelEventId' in d]
                                print("hfbgdshfvb",id_list)
                                if id in id_list:
                                    pass
                                else:
                                    cv2.imwrite(f"/home/milestone/Downloads/output/aa/{id}.jpg",frame)
                                    img_64 = f"data:image/jpeg;base64/{str(numpy_array_to_base64(frame))}" 
                                    temp = {
                                            "wheelEventId": id,
                                            "wheelSequence": id,
                                            "wheelTimestamp": str(datetime.utcnow().isoformat()),
                                            "wheelCameraId": "mcc-Alt-01-01",
                                            "wheelResult": target_folder.capitalize(),
                                            "wheelImageBase64": vehicleImageBase64
                                            }
                                    wheels.append(temp)
                            new_rows = pd.DataFrame([{'wheel_id': i, 'class': target_folder,"Year":current_year,"Month":current_month,"Day":current_day,"Time":current_time} for i in track_ids if i not in df['wheel_id'].values])
            
            
                            #         # Concatenate the original DataFrame and the new rows
                            df = pd.concat([df, new_rows], ignore_index=True)
                out.write(frame)
                frame = cv2.resize(frame, (640,480))
                # main_data["vehicleImageBase64"] = f"data:image/jpeg;base64/{numpy_array_to_base64(frame)}"
                print("time duration",time_duration)
                if count%time_duration==0:
                                
                    result_list = [d['wheelResults'] for d in wheels if 'wheelResults' in d]
                    # main_data["vehicleImageBase64"] = ""
                    if "unsafe" in result_list:
                        main_data["vehicleResult"] = "Unsafe"
                    else:
                        main_data["vehicleResult"] = "Safe"
                    pretty_json_string = json.dumps(main_data, indent=4)  
                    #print(pretty_json_string)
            
                    data_queue.put(pretty_json_string)
                    count = 0
                # elif count == total_framess:
                #     data_queue.put(df) 
                cv2.imshow('frame', frame)
                
                if cv2.waitKey(1) == ord('q'):
                    break        
                 
                
                      
                else:
                    print("count",count)
                    continue
                
            else:
                break
        
        # data_queue.put(None)
        # print()
        # write_base64_to_file(pretty_json_string)
        cap.release()
        out.release()
        cv2.destroyAllWindows()

        
def show_info(data_queue, done_flag,ab):
    
    # df = pd.DataFrame()
    while not done_flag.is_set() or not data_queue.empty():
        if not data_queue.empty():
            payload = data_queue.get()
            ab += 1
            url = "https://jsonhandler.azurewebsites.net/api/http_trigger"
            headers = {
                    'Content-Type': 'application/json'
                    }

            response = requests.request("POST", url, headers=headers, data=payload)
          
            print("dnfcjksdbfjksdnfjkn",response.text)
           
            
            # item = 
    #         df = item
    #         print(f"{ab} From Queue:", item)

    #         ab += 1
    #     # time.sleep(10)
    #         # json_data = item.to_json(orient='records')
            
    #         print("dkshvjdks",json_data)
            
        
        
if __name__ == "__main__":
    
    ab = 1
    ############################################################################3

    parser = argparse.ArgumentParser(description="",formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--video", type = str, default = None,
                        help="path to input video file")
    parser.add_argument("--source", type = str, default = None,
                        help="path to input video file")
    parser.add_argument("--results", type = str, default = None,
                        help="path to input video file")

    parser.add_argument('--rtsp', dest='use_rtsp',
                        help='use IP CAM (remember to also set --uri)',
                        action='store_true')
    parser.add_argument('--uri', dest='rtsp_uri',
                        help='RTSP URI, e.g. rtsp://192.168.1.64:554',
                        default=None, type=str)
    parser.add_argument('--latency', dest='rtsp_latency',
                        help='latency in ms for RTSP [200]',
                        default=200, type=int)
    parser.add_argument('--usb', dest='use_usb',
                        help='use USB webcam (remember to also set --vid)',
                        action='store_true')
    parser.add_argument('--vid', dest='video_dev',
                        help='device # of USB webcam (/dev/video?) [1]',
                        default=1, type=int)
    parser.add_argument('--width', dest='image_width',
                        help='image width [1920]',
                        default=1920, type=int)
    parser.add_argument('--height', dest='image_height',
                        help='image height [1080]',
                        default=1080, type=int)
        
    args = parser.parse_args()
    data_queue = multiprocessing.Queue()
    done_flag = multiprocessing.Event()
    # Create processes for yolo_pipeline and show_info
    yolo_process = multiprocessing.Process(target=YOLO_pipeline, args=(args, data_queue))
    
   
    show_info_process = multiprocessing.Process(target=show_info, args=(data_queue,done_flag,ab))
    # # Start both processes
    yolo_process.start()
    show_info_process.start()
    
    
    yolo_process.join()
    done_flag.set()
    show_info_process.join()
    

    
   
