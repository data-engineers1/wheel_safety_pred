from ultralytics import YOLO
import cv2
import os
from pathlib import Path
import time
import numpy as np
from  ultralytics.utils.plotting import Annotator
model = YOLO("/home/milestone/Downloads/yolov8_detect.pt")
model_seg= YOLO("/home/milestone/Downloads/yolov8_seg.pt")
source = r"/home/milestone/Downloads/output/oo/"

def classify(img_detail):
    bbox=img_detail['bbox']
    classes=img_detail['classes']
    confidence=img_detail['confidence']
    height=img_detail['height']
    width=img_detail['width']
    my_dict = {}
    result=[]
    target_folder=None
    for i in zip(bbox,classes,confidence):
        bx=i[0][0]
        by=i[0][1]
        bw=i[0][2]
        bh=i[0][3]

        
        bbox_center_x=bx+(bw/2)
        bbox_center_y=by+(bh/2)
        image_center_x=width/2
        image_center_y=height/2
        distance = np.sqrt(((bbox_center_x-image_center_x)**2)+((bbox_center_y-image_center_y)**2))
        
        classes = str(i[1][0]).split('.')[0]
        confidence = i[2][0]
        tuple_for_iteration = (classes, confidence)
        my_dict[distance]=tuple_for_iteration
    my_dict=dict(sorted(my_dict.items()))
    
    #7 closet nuts 
    first_7_items = dict(list(my_dict.items())[:7])
    #5 hightest confidence
    dictt=dict(list(sorted(first_7_items.items(), key=lambda item: item[1][1]))[:5])

    final_classes = list(map(lambda x: str(x[0]), dictt.values()))
    
    if any(element == 1 for element in classes):
        target_folder="Unsafe"
    else:
        target_folder="safe"
    
    return target_folder
      
     
      
            
        
        
 
        
for image_name in os.listdir(source):
    frame = cv2.imread(source+image_name)
    results = model(frame, conf = 0.40)
    
    classes=[]
    bbox=[]
    img_detail={}
    confi=[]    
    for result in results:
        annotator = Annotator(frame)
        
        boxes = result.boxes
        print("box1",boxes)
        for box in boxes:
            
            box1 = box.xyxy.tolist()
            
            conf = box.conf.tolist()
            cls = box.cls.tolist()
            
            # left, top, width, height = convert_to_top_left(box[0],box[1],box[2],box[3])
        
            # print(left, top, width, height)
    # break
    x, y, w, h=None,None,None,None
    for result in results:
        annotator = Annotator(frame)
        boxes = result.boxes
        
        target_folder=None
        for box in boxes:
            
            b = box.xyxy[0]
            bb = box.xyxy[0].tolist()
            bbox.append(bb)
            c = box.cls
            clss = box.cls.tolist()
            
     
            x, y, w, h = int(bb[0]),int(bb[1]),int(bb[2]),int(bb[3])

    croped=frame[y:h,x:w]
    print(croped)
    croped = cv2.resize(croped, (640,480))
    # Naming a window 
    cv2.namedWindow("Resized_Window", cv2.WINDOW_NORMAL) 
    
    # Using resizeWindow() 
    cv2.resizeWindow("Resized_Window", 300, 700) 
    
    # Displaying the image 
    cv2.imshow("Resized_Window", croped) 
    cv2.waitKey(0)
