#!/usr/bin/env python

import sys
import gi

gi.require_version('Gst', '1.0')
gi.require_version('GstRtspServer', '1.0')
from gi.repository import Gst, GstRtspServer, GObject, GLib

loop = GLib.MainLoop()
Gst.init(None)

class TestRtspMediaFactory(GstRtspServer.RTSPMediaFactory):
    def __init__(self):
        GstRtspServer.RTSPMediaFactory.__init__(self)

    def do_create_element(self, url):
        src_demux = "filesrc location=/home/milestone/Downloads/img-7598_o.mp4 ! qtdemux name=demux"
        h264_transcode = "demux.video_0"
        pipeline = "{0} {1} ! queue ! rtph264pay name=pay0 config-interval=1 pt=96".format(src_demux, h264_transcode)

        try:
            print("Creating GStreamer pipeline:")
            print(pipeline)

            # Attempt to create the GStreamer pipeline
            gst_pipeline = Gst.parse_launch(pipeline)

            if gst_pipeline:
                print("GStreamer pipeline created successfully")
            else:
                print("Failed to create GStreamer pipeline")
        except Exception as e:
            print("An exception occurred while creating the GStreamer pipeline:".format(e))

        return gst_pipeline

class GstreamerRtspServer():
    def __init__(self):
        self.rtspServer = GstRtspServer.RTSPServer()
        factory = TestRtspMediaFactory()
        factory.set_shared(True)
        mountPoints = self.rtspServer.get_mount_points()
        mountPoints.add_factory("/stream1", factory)
        self.rtspServer.attach(None)

if __name__ == '__main__':
    try:
        # Set GStreamer debug level (0 = no debugging, 3 = maximum debugging)
        Gst.debug_set_default_threshold(Gst.DebugLevel.DEBUG)

        s = GstreamerRtspServer()
        loop.run()
    except Exception as e:
        print("An exception occurred:".format(e))
