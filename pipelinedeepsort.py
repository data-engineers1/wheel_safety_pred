from ultralytics import YOLO
import cv2
from pathlib import Path
import time
import numpy as np
import pandas as pd
from deep_sort.deep_sort import DeepSort


from classification import classify,write_text,prediction_result

model = YOLO("models/yolov8_detect.pt")
model_seg = YOLO("models/yolov8_seg.pt")

deep_sort_weights = 'deep_sort/deep/checkpoint/ckpt.t7'
tracker = DeepSort(model_path=deep_sort_weights, max_age=250)


############################################################################3
import cv2,sys,subprocess
import argparse
import os
parser = argparse.ArgumentParser(description="",formatter_class=argparse.ArgumentDefaultsHelpFormatter)
# parser.add_argument("--video", type = str, default = None,
#                     help="path to input video file")
# parser.add_argument("--cam", type = int, default = None,
#                     help="webcam or usbcam live feed")
# parser.add_argument("--rtsp_url", type = str, default = None,
#                     help="rstp camera's live feed")

parser.add_argument("--video", type = str, default = None,
                    help="path to input video file")
parser.add_argument("--source", type = str, default = None,
                    help="path to input video file")
parser.add_argument("--results", type = str, default = None,
                    help="path to input video file")

parser.add_argument('--rtsp', dest='use_rtsp',
                    help='use IP CAM (remember to also set --uri)',
                    action='store_true')
parser.add_argument('--uri', dest='rtsp_uri',
                    help='RTSP URI, e.g. rtsp://192.168.1.64:554',
                    default=None, type=str)
parser.add_argument('--latency', dest='rtsp_latency',
                    help='latency in ms for RTSP [200]',
                    default=200, type=int)
parser.add_argument('--usb', dest='use_usb',
                    help='use USB webcam (remember to also set --vid)',
                    action='store_true')
parser.add_argument('--vid', dest='video_dev',
                    help='device # of USB webcam (/dev/video?) [1]',
                    default=1, type=int)
parser.add_argument('--width', dest='image_width',
                    help='image width [1920]',
                    default=1920, type=int)
parser.add_argument('--height', dest='image_height',
                    help='image height [1080]',
                    default=1080, type=int)
    
args = parser.parse_args()

selection_dict = {'img': None, 'points selected': []}
bboxes = []

def open_cam_rtsp(uri, width, height, latency):
    gst_str = ('rtspsrc location={} latency={} ! '
               'rtph264depay ! h264parse ! omxh264dec ! '
               'nvvidconv ! '
               'video/x-raw, width=(int){}, height=(int){}, '
               'format=(string)BGRx ! '
               'videoconvert ! appsink').format(uri, latency, width, height)
    return cv2.VideoCapture(gst_str, cv2.CAP_GSTREAMER)


def open_cam_usb(dev, width, height):
    # We want to set width and height here, otherwise we could just do:
    #     return cv2.VideoCapture(dev)
    # v4l2src device=/dev/video{} ! '
    #                'video/x-raw, width=(int){}, height=(int){} ! '
    #                'videoconvert ! appsink
               
    gst_str = ('v4l2src device=/dev/video{} ! '
               'image/jpeg, framerate=30/1, width=(int){}, height=(int){} ! '
               'jpegparse ! '
               'jpegdec ! videoconvert ! appsink').format(dev, width, height)
    return cv2.VideoCapture(gst_str, cv2.CAP_GSTREAMER)


def open_cam_onboard(width, height):
    gst_elements = str(subprocess.check_output('gst-inspect-1.0'))
    if 'nvcamerasrc' in gst_elements:
        # On versions of L4T prior to 28.1, add 'flip-method=2' into gst_str
        gst_str = ('nvcamerasrc ! '
                   'video/x-raw(memory:NVMM), '
                   'width=(int)2592, height=(int)1458, '
                   'format=(string)I420, framerate=(fraction)30/1 ! '
                   'nvvidconv ! '
                   'video/x-raw, width=(int){}, height=(int){}, '
                   'format=(string)BGRx ! '
                   'videoconvert ! appsink').format(width, height)
    elif 'nvarguscamerasrc' in gst_elements:
        gst_str = ('nvarguscamerasrc ! '
                   'video/x-raw(memory:NVMM), '
                   'width=(int)1920, height=(int)1080, '
                   'format=(string)NV12, framerate=(fraction)30/1 ! '
                   'nvvidconv flip-method=2 ! '
                   'video/x-raw, width=(int){}, height=(int){}, '
                   'format=(string)BGRx ! '
                   'videoconvert ! appsink').format(width, height)
    else:
        raise RuntimeError('onboard camera source not found!')
    return cv2.VideoCapture(gst_str, cv2.CAP_GSTREAMER)


cap=None
source_images=False

out_results=args.results
if not out_results.endswith("/"):
        out_results = args.results+'/'
        
        
        
if args.video:
    cap = cv2.VideoCapture(args.video)
    width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
    height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)

elif args.source:
    if os.path.isdir(args.source):
        source_images = True
    else:
        source_images = False

elif args.use_rtsp:
# rtsp_uri = "rtsp://localhost:8554/stream1"
# if rtsp_uri:
    
    cap = open_cam_rtsp(args.rtsp_uri,
                                args.image_width,
                                args.image_height,
                                args.rtsp_latency)
    
    width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
    height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
    print("dshvjksdbhvjkdh",height)

elif args.use_usb:
    cap = open_cam_usb(args.video_dev,
                        args.image_width,
                        args.image_height)
    width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
    height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)

else:
    cap = open_cam_onboard(args.image_width,
                            args.image_height)
    width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
    height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)

# For Video
if cap:
    if not cap.isOpened():
        sys.exit('Failed to open camera!')

    cv2.destroyAllWindows()
    ############################################################################
    frame_rate = int(cap.get(cv2.CAP_PROP_FPS))
    video_name = args.video.rsplit("/")[-1]

    
        
    output_path = out_results+video_name
    frame_width = int(cap.get(3))
    frame_height = int(cap.get(4))
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    out = cv2.VideoWriter(output_path, fourcc, frame_rate, (frame_width, frame_height))
    df = pd.DataFrame(columns=['wheel_id', 'class'])
    d=[]
    count=0
    while cap.isOpened():
        success, frame = cap.read()
        if frame is not None:
            og_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frame = og_frame.copy()
        count=count+1
        if success:
            results = model(frame, conf = 0.40)            
            height,width,my_dict=prediction_result(frame,results,model,model_seg)
            target_folder=classify(my_dict)
            image_with_text=write_text(height,width,frame,target_folder)
            og_frame = cv2.cvtColor(image_with_text, cv2.COLOR_BGR2RGB)
            
            class_names = ['wheel']
            for result in results:
                boxes = result.boxes  # Boxes object for bbox outputs
                probs = result.probs  # Class probabilities for classification outputs
                cls = boxes.cls.tolist()  # Convert tensor to list
                xyxy = boxes.xyxy
                conf = boxes.conf
                xywh = boxes.xywh  # box with xywh format, (N, 4)
            pred_cls = np.array(cls)
            conf = conf.detach().cpu().numpy()
            xyxy = xyxy.detach().cpu().numpy()
            bboxes_xywh = xywh
            bboxes_xywh = xywh.cpu().numpy()
            bboxes_xywh = np.array(bboxes_xywh, dtype=float)
            tracks = tracker.update(bboxes_xywh, conf, og_frame)
            for track in tracker.tracker.tracks:
                track_id = track.track_id
                d.append(track_id)
                hits = track.hits
                x1, y1, x2, y2 = track.to_tlbr()  # Get bounding box coordinates in (x1, y1, x2, y2) format
                w = x2 - x1  # Calculate width
                h = y2 - y1  # Calculate height

                # Set color values for red, blue, and green
                red_color = (0, 0, 255)  # (B, G, R)
                blue_color = (255, 0, 0)  # (B, G, R)
                green_color = (0, 255, 0)  # (B, G, R)

                # Determine color based on track_id
                color_id = track_id % 3
                if color_id == 0:
                    color = red_color
                elif color_id == 1:
                    color = blue_color
                else:
                    color = green_color

                cv2.rectangle(og_frame, (int(x1), int(y1)), (int(x1 + w), int(y1 + h)), color, 2)

                text_color = (0, 0, 0)  # Black color for text
                cv2.putText(og_frame, f"{track_id}", (int(x1) + 10, int(y1) - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, text_color, 1, cv2.LINE_AA)
            
            # print('gdfhdg',target_folder)
            # print("vhjkfdshvj",track_ids)
            # for i in track_ids:
            #     print("nhcjkdhbcvjkhbdscv",i,target_folder)
                
            #     d.append(i)
            
          
            # Create a DataFrame with the new rows to be appended
            # if target_folder != '-':
            #     new_rows = pd.DataFrame([{'wheel_id': i, 'class': target_folder} for i in track_ids if i not in df['wheel_id'].tolist()])

            #     # Concatenate the original DataFrame and the new rows
            #     df = pd.concat([df, new_rows], ignore_index=True)

                
                    
            frame = cv2.resize(og_frame, (640,480))
            cv2.imshow('frame', og_frame)
            if cv2.waitKey(1) == ord('q'):
                break        
            # out.write(image_with_text)
            
            
        else:
            break
        print("count",count)
    
    cap.release()
    out.release()
    cv2.destroyAllWindows()
    print("sgfvsdhvfsdhgvh",d)
    
# For Images
else: 
       
    if source_images:
        #For Images directory
        sour = args.source
        if not sour.endswith('/'):
            sour=args.source+'/'
       
        for image_name in os.listdir(sour):
            print(image_name)
            frame = cv2.imread(sour+image_name)
            results = model(frame, conf = 0.40)
            height,width,my_dict=prediction_result(frame,results,model,model_seg)
            target_folder=classify(my_dict)
            image_with_text=write_text(height,width,frame,target_folder)
            cv2.imwrite(out_results+image_name,image_with_text)
           

            
    else:
        #For Image 
        print(source_images)
        frame = cv2.imread(args.source)
        image_name = args.source.rsplit('/')[-1]
        results = model(frame, conf = 0.40)
        height,width,my_dict=prediction_result(frame,results,model,model_seg)
        target_folder=classify(my_dict)
        image_with_text=write_text(height,width,frame,target_folder)
        cv2.imwrite(out_results+image_name,image_with_text)
    